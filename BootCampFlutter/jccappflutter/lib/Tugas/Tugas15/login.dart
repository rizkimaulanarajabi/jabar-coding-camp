import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  get textAlign => null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: EdgeInsets.all(25.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 50),
          Text.rich(
            TextSpan(
              text: 'Sanber Flutter',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.lightBlueAccent),
            ),
            style: TextStyle(fontSize: 30),
          ),
          SizedBox(height: 10),
          SizedBox(
            height: 100,
            child: GridView.count(
              padding: EdgeInsets.zero,
              crossAxisCount: 1,
              childAspectRatio: 4.800,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              physics: NeverScrollableScrollPhysics(),
              children: [Image.asset('assets/img/flutter.png')],
            ),
          ),
          SizedBox(height: 30),
          TextField(
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              hintText: 'Username',
            ),
          ),
          SizedBox(height: 25),
          TextField(
            decoration: InputDecoration(
              fillColor: Colors.blue,
              filled: true,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              hintText: 'Password',
              hintStyle: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
          SizedBox(height: 20),
          Text.rich(
            TextSpan(
              text: 'Forgot Password',
              style: TextStyle(
                  fontWeight: FontWeight.bold, color: Colors.lightBlueAccent),
            ),
            style: TextStyle(fontSize: 15),
          ),
          SizedBox(height: 25),
          // ignore: deprecated_member_use
          RaisedButton(
            onPressed: () {
              Navigator.pushNamed(context, '/home');
            },
            child: Text('login'),
          ),
          SizedBox(height: 30),
          Text.rich(
            TextSpan(
                text: 'Does not have account ?',
                style: TextStyle(fontWeight: FontWeight.bold),
                children: [
                  TextSpan(
                    text: ' Sign in',
                    style: TextStyle(
                        fontWeight: FontWeight.normal, color: Colors.blue),
                  ),
                ]),
            style: TextStyle(fontSize: 12),
          ),
        ],
      ),
    ));
  }
}
