import 'dart:io';

void main() {
  print('Looping Pertama');
  var loop1 = 2;
  while (loop1 <= 20) {
    print(loop1.toString() + " - I Love Coding ");
    loop1 += 2;
  }
  print('Looping Kedua');
  var loop2 = 20;
  while (loop2 >= 2) {
    print(loop2.toString() + " - I Will Become Mobile Developer ");
    loop2 -= 2;
  }
  // NO  1 SELESAI

  for (var x = 1; x <= 20; x++) {
    if (x % 2 == 0) {
      print(x.toString() + " - Berkualitas");
    } else if (x % 3 == 0) {
      print(x.toString() + " - I Love Coding");
    } else {
      print(x.toString() + " - Santai");
    }
  }
  //NO 2 SELESAI

  var n = 4;
  var m = 7;
  print('---Membuat Persegi Panjang---');
  for (var i = 1; i <= n; i++) {
    for (var j = 1; j <= m; j++) {
      stdout.write('#');
    }
    print('');
  }
  // NO 3 SELESAI
  var a = 7;
  print('---Membuat Tangga---');
  for (var b = 1; b <= a; b++) {
    for (var c = 0; c < b; c++) {
      stdout.write('#');
    }
    print('');
  }
  // NO 4 SELESAI
}
