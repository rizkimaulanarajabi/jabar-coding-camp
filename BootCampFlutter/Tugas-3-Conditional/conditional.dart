import 'dart:io';

void main() {
  stdout.write("Mau diinstal Aplikasi ? (y/t): ");
  String jawaban = stdin.readLineSync()!;
  var hasil = (jawaban == 'y' || jawaban == 'Y')
      ? "Anda Akan Menginstal Aplikasi Dart"
      : "Aborted";

  print("$hasil");
//no 1 Selesai

  stdout.write("Masukan Nama Anda: ");
  String nama = stdin.readLineSync()!;
  stdout.write("Peran: ");
  String peran = stdin.readLineSync()!;
  if (nama == "")
    print("Nama Harus Diisi");
  else if (peran == "") {
    print(
        "Selamat datang di Dunia Werewolf,$nama Pilih Peranmu untuk memulai game");
  } else if (peran == "Penyihir" || peran == "penyihir") {
    print(
        "Selamat datang di Dunia Werewolf $nama,  Halo $peran $nama, kamu dapat melihat siapa yang menjadi werewolf!");
  } else if (peran == "Guardian" || peran == "guardian") {
    print(
        "Selamat datang di Dunia Werewolf $nama, Halo $peran $nama , kamu akan membantu melindungi temanmu dari serangan werewolf");
  } else if (peran == "Werewolf" || peran == "werewolf") {
    print(
        "Selamat datang di Dunia Werewolf $nama,Halo $peran $nama, Kamu akan memakan mangsa setiap malam!");
  } else {
    print("Selamat datang di Dunia Werewolf $nama, Peranmu Sebagai $peran");
  }
  //no 2 Selesai

  stdout.write("Quotes Hari Senin - Jumat: ");
  String namahari = stdin.readLineSync()!;
  String hari = '$namahari';
  switch (hari) {
    case 'Senin':
      {
        print(
            'Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.');
        break;
      }
    case 'Selasa':
      {
        print(
            'Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.');
        break;
      }
    case 'Rabu':
      {
        print(
            'Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.');
        break;
      }
    case 'Kamis':
      {
        print(
            'Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.');
        break;
      }
    case 'Jumat':
      {
        print('Hidup tak selamanya tentang pacar.');
        break;
      }
    case 'Sabtu':
      {
        print('Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.');
        break;
      }
    case 'Minggu':
      {
        print(
            'Hanya seseorang yang takut yang bisa bertindak berani. Tanpa rasa takut itu tidak ada apapun yang bisa disebut berani.');
        break;
      }
    default:
      {
        print('Hari Tidak ada - Tidak ada Quotes');
      }
  }
  //no 3 Selesai

  var tanggal = 21;
  var bulan = 4;
  var tahun = 1945;
  switch (bulan) {
    case 1:
      {
        print('$tanggal Januari $tahun');
        break;
      }
    case 2:
      {
        print('$tanggal Februari $tahun');
        break;
      }
    case 3:
      {
        print('$tanggal Maret $tahun');
        break;
      }
    case 4:
      {
        print('$tanggal April $tahun');
        break;
      }
    case 5:
      {
        print('$tanggal Mei $tahun');
        break;
      }
    case 6:
      {
        print('$tanggal Juni $tahun');
        break;
      }
    case 7:
      {
        print('$tanggal Juli $tahun');
        break;
      }
    case 8:
      {
        print('$tanggal Agustus $tahun');
        break;
      }
    case 9:
      {
        print('$tanggal September $tahun');
        break;
      }
    case 10:
      {
        print('$tanggal Oktober $tahun');
        break;
      }
    case 11:
      {
        print('$tanggal November $tahun');
        break;
      }
    case 12:
      {
        print('$tanggal Desember $tahun');
        break;
      }
    default:
      {
        print('Bulan Tidak ada');
      }
  }
  //no 4 selesai
}
