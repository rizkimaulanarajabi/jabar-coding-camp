void main(List<String> args) async {
  var l = Lyric();

  print(l.name);
  await l.line1();
  print(l.name);
  await l.line2();
  print(l.name);
  await l.line3();
  print(l.name);
  await l.line4();
  print(l.name);
}

class Lyric {
  String name = "Ready to sing";
  Future<void> line1() async {
    await Future.delayed(Duration(seconds: 5));
    name = "Pernahkah Kau Merasa";
    print("");
  }

  Future<void> line2() async {
    await Future.delayed(Duration(seconds: 3));
    name = "Pernahkah Kau Merasa .... ";
    print("");
  }

  Future<void> line3() async {
    await Future.delayed(Duration(seconds: 2));
    name = "Pernahkah Kau Merasa ..";
    print("");
  }

  Future<void> line4() async {
    await Future.delayed(Duration(seconds: 1));
    name = "Hatimu Hampa, Pernahkah Kau Merasa Hatimu Kosong";
    print("");
  }
}
