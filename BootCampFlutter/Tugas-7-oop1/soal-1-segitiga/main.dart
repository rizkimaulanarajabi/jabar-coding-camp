void main() {
  Segitiga1 segitiga;
  double luasSegitiga;

  segitiga = new Segitiga1();
  segitiga.setengah = 0.5;
  segitiga.alas = 20.0;
  segitiga.tinggi = 30.0;
  luasSegitiga = segitiga.hitungLuas();
  print(luasSegitiga);
}

class Segitiga1 {
  double setengah = 0.5;
  double alas = 20.0;
  double tinggi = 30.0;
  double hitungLuas() {
    return this.setengah * alas * tinggi;
  }
}
