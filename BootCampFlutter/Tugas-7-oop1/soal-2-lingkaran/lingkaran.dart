class Lingkaran1 {
  double _phi = 3.14;
  double _JariJari = 20;
  void setPhi(double value) {
    if (value < 0) {
      value *= -1;
    }
    _phi = value;
  }

  double getPhi() {
    return _phi;
  }

  void setJariJari(double value) {
    if (value < 0) {
      value *= -1;
    }
    _JariJari = value;
  }

  double getJariJari() {
    return _JariJari;
  }

  double hitungLuas() {
    return this._phi * _JariJari * _JariJari;
  }
}
