import 'lingkaran.dart';

void main(List<String> args) {
  Lingkaran1 lingkaran;
  double luasLingkaran;

  lingkaran = new Lingkaran1();
  lingkaran.setPhi(3.14);
  lingkaran.setJariJari(20);

  luasLingkaran = lingkaran.hitungLuas();
  print(luasLingkaran);
}
