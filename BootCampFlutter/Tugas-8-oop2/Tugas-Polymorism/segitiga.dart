import 'bangun_datar.dart';

class Segitiga extends Bangundatar {
  late double b;
  late double a;
  late double t;

  Segitiga(double b, double a, double t) {
    this.b = b;
    this.a = a;
    this.t = t;
  }

  double convert() {
    return a * b * t;
  }

  double convert2() {
    return a + b + t;
  }
}
