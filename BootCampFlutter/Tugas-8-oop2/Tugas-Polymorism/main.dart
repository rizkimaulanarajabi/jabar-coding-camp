import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main(List<String> args) {
  Bangundatar bangundatar = new Bangundatar();
  Segitiga segitiga = new Segitiga(0.5, 4, 10);
  Persegi persegi = new Persegi(4, 2, 2);
  Lingkaran lingkaran = new Lingkaran(3.14, 2, 4);

  bangundatar.convert();

  print("Luas Segitiga : ${segitiga.convert()}");
  print("Keliling Segitiga : ${segitiga.convert2()}");
  print("Luas Persegi : ${persegi.convert()}");
  print("Keliling Persegi : ${persegi.convert2()}");
  print("Luas Lingkaran : ${lingkaran.convert()}");
  print("Diameter Lingkaran : ${lingkaran.convert2()}");
}
