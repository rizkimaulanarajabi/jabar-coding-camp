import 'bangun_datar.dart';

class Lingkaran extends Bangundatar {
  late double phi;
  late double d;
  late double r;

  Lingkaran(double phi, double d, double r) {
    this.phi = phi;
    this.d = d;
    this.r = r;
  }

  double convert() {
    return phi * r * r;
  }

  double convert2() {
    return d * phi * r;
  }
}
