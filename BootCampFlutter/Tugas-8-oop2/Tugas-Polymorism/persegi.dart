import 'bangun_datar.dart';

class Persegi extends Bangundatar {
  late double s;
  late double s1;
  late double s2;

  Persegi(double s, double s1, double s2) {
    this.s = s;
    this.s1 = s1;
    this.s2 = s2;
  }

  double convert() {
    return s1 * s2;
  }

  double convert2() {
    return s * s1;
  }
}
