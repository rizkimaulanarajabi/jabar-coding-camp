import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  Armortitan r = Armortitan();
  Attacktitan a = Attacktitan();
  Beasttitan b = Beasttitan();
  Human h = Human();

  r.levelpoint = 4;
  a.levelpoint = 6;
  b.levelpoint = 9;
  h.levelpoint = 10;

  print("level point Armor Titan : ${r.levelpoint}");
  print("level point Attack Titan : ${a.levelpoint}");
  print("level point Beast Titan : ${b.levelpoint}");
  print("level point Human : ${h.levelpoint}");

  print("Armor Titan : " + r.terjang());
  print("Attack Titan : " + a.punch());
  print("Beast Titan : " + b.lempar());
  print("Human : " + h.killAlltitan());
}
